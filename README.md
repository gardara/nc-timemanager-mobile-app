# Timemanager App

> THIS PROJECT IS NOT MAINTAINED ANYMORE.
> If someone makes the project work again (support NC27 + Flutter/Dart3), I can update the app in the playstore.

Powered by [Nextcloud](https://nextcloud.com/).

<img src="assets/launcher/icon.png" alt="drawing" width="200"/>

A timemanager app (Android + iOS) for
Nextcloud's [Timemanager](https://apps.nextcloud.com/apps/timemanager) app.

As of now, the app must be built by yourself.

## Donate

<a href="https://ko-fi.com/joleaf" target="_blank"><img src="https://uploads-ssl.webflow.com/5c14e387dab576fe667689cf/61e11d503cc13747866d338b_Button-2-p-800.png" alt="Buy Me A Pizza" height="41" width="174"></a>

## Getting Started

This application is built with [Flutter](https://flutter.dev/).
[Install and configure flutter](https://flutter.dev/docs/get-started/install), then clone this
repository. Then run ```flutter run --no-sound-null-safety``` for a debug preview on a started
emulator, a connected Android or iOS device.

Depending on the changes you made you may also need to run these commands before running the
application:

```flutter clean```

```flutter pub get```

```flutter pub run flutter_launcher_icons:main```

## Screenshots

<img src="screenshots/screen_00001.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00002.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00003.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00004.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00005.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00006.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00007.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00008.jpg" alt="drawing" width="215"/>
<img src="screenshots/screen_00009.jpg" alt="drawing" width="215"/>

## Features

- Create/Update/Delete your Clients/Projects/Tasks/Times
- Dashboard with your last tasks
- Charts for some statistics
- Start a timer and track your time for a task
- Export your times as CSV
- Overview and of the time entries (+filter by timespan)

## Future features

- Missing something? Create
  an [Issue](https://gitlab.com/joleaf/nc-timemanager-mobile-app/-/issues/new) :)
