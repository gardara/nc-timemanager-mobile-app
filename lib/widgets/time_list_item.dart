import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../helper/duration_format.dart';
import '../helper/i18n_helper.dart';
import '../model/abstract_timemanager_object.dart';
import '../model/time.dart';
import '../provider/settings_provider.dart';
import '../provider/timemanager_provider.dart';

class TimeListItem extends StatelessWidget {
  final Time time;
  final Function updateTime;
  final bool showAllInfos;

  TimeListItem(this.time, this.updateTime, {this.showAllInfos = false});

  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);
    final settings = Provider.of<SettingsProvider>(context, listen: false);
    return Dismissible(
      key: ValueKey(time.uuid),
      direction: DismissDirection.horizontal,
      background: Container(
        padding: EdgeInsets.only(left: 20),
        alignment: Alignment.centerLeft,
        color: Colors.blueGrey,
        child: Icon(
          Icons.copy,
          color: Colors.white,
          size: 40,
        ),
      ),
      secondaryBackground: Container(
        padding: EdgeInsets.only(right: 20),
        alignment: Alignment.centerRight,
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
      ),
      onDismissed: (direction) {
        if (direction == DismissDirection.endToStart) {
          final tm = Provider.of<TimemanagerProvider>(context, listen: false);
          tm.delete(time);
          tm.sync();
        }
      },
      confirmDismiss: (direction) async {
        if (direction == DismissDirection.startToEnd) {
          await updateTime(
              context,
              time.task,
              Time.copy(
                CopyBehaviour.createNew,
                tm,
                time,
                start: time.start,
                end: time.end,
                task_uuid: time.task_uuid,
              ),
              true);
          return false;
        } else {
          return showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
              title: Text('dialog.are_you_sure'.tl(context)),
              content: Text('dialog.delete_item'.tl(context)),
              actions: [
                TextButton(
                  child:
                      Text(MaterialLocalizations.of(context).cancelButtonLabel),
                  onPressed: () => Navigator.of(ctx).pop(false),
                ),
                TextButton(
                  child: Text(MaterialLocalizations.of(context).okButtonLabel),
                  onPressed: () => Navigator.of(ctx).pop(true),
                ),
              ],
            ),
          );
        }
      },
      child: ListTile(
        leading: IconButton(
            icon: Icon(
              time.paymentStatus == 'paid'
                  ? Icons.check_box
                  : Icons.check_box_outline_blank_sharp,
            ),
            onPressed: () {
              tm.update(
                Time.copy(
                  CopyBehaviour.updateCurrent,
                  tm,
                  time,
                  start: time.start,
                  end: time.end,
                  paymentStatus:
                      time.paymentStatus == 'paid' ? 'unpaid' : 'paid',
                ),
              );
              tm.sync();
            }),
        title: Text((showAllInfos
                ? time.client.name +
                    ' - ' +
                    time.project.name +
                    ' - ' +
                    time.task.name +
                    '\n'
                : '') +
            time.note),
        subtitle: Text(
          DateFormat.yMMMMEEEEd(Localizations.localeOf(context).languageCode)
              .format(time.start),
        ),
        trailing: settings.recordingType == RecordingType.Duration
            ? Text(time.duration.toFormattedString(
                showDays: Provider.of<SettingsProvider>(context, listen: false)
                    .showDays))
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(DateFormat.Hm(
                          Localizations.localeOf(context).languageCode)
                      .format(time.start)),
                  Text('-'),
                  Text(DateFormat.Hm(
                          Localizations.localeOf(context).languageCode)
                      .format(time.end))
                ],
              ),
        onLongPress: () async => await updateTime(context, time.task, time),
      ),
    );
  }
}
