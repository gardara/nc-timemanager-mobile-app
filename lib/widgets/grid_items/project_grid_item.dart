import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../helper/i18n_helper.dart';
import '../../helper/duration_format.dart';
import '../../helper/date_time_helper.dart';
import '../../helper/share_times.dart';
import '../../model/project.dart';
import '../../provider/settings_provider.dart';
import '../../provider/timemanager_provider.dart';
import '../../screens/task_overview_screen.dart';

import './timemanager_object_grid_item.dart';

class ProjectGridItem extends StatelessWidget {
  final String projectId;
  final Function() onLongPress;
  final bool isRoot;

  const ProjectGridItem(
    this.projectId,
    this.onLongPress, {
    this.isRoot = false,
  });

  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<SettingsProvider>(context, listen: false);
    final tm = Provider.of<TimemanagerProvider>(context);
    final project = tm.get<Project>(projectId)!;
    return TimemanagerObjectGridItem(
      timeMangerObject: project,
      onTap: () => Navigator.of(context).pushNamed(
        TaskOverviewScreen.routeName,
        arguments: project.uuid,
      ),
      onLongPress: onLongPress,
      leadingIcon: Icon(Icons.article_outlined, size: 30),
      titleText: project.name + (isRoot ? ' - ' + project.client.name : ''),
      subtitle: Container(
        height: 90,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text('general.tasks'.tl(context) +
                ': ' +
                project.tasks.length.toString()),
            Text('general.today'.tl(context) +
                ': ' +
                project
                    .getDurationBetween(DateTime.now().getStartOfDay(),
                        DateTime.now().getEndOfDay())
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.this_week'.tl(context) +
                ': ' +
                project
                    .getDurationBetween(
                        DateTime.now()
                            .getStartOfWeek(startOfWeek: settings.startOfWeek),
                        DateTime.now()
                            .getEndOfWeek(startOfWeek: settings.startOfWeek))
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.last_week'.tl(context) +
                ': ' +
                project
                    .getDurationBetween(
                        DateTime.now().getStartOfWeek(
                            weekOffset: -1, startOfWeek: settings.startOfWeek),
                        DateTime.now().getEndOfWeek(
                            weekOffset: -1, startOfWeek: settings.startOfWeek))
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.total'.tl(context) +
                ': ' +
                project.duration
                    .toFormattedString(showDays: settings.showDays)),
          ],
        ),
      ),
      onShare: () => ShareTimes.share(project.times.toList(), context),
    );
  }
}
