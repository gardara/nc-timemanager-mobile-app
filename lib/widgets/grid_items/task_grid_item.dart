import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './timemanager_object_grid_item.dart';
import '../../helper/date_time_helper.dart';
import '../../helper/duration_format.dart';
import '../../helper/i18n_helper.dart';
import '../../helper/share_times.dart';
import '../../model/task.dart';
import '../../provider/settings_provider.dart';
import '../../provider/task_time_tracker_provider.dart';
import '../../provider/timemanager_provider.dart';
import '../../widgets/time_entries_modal.dart';

class TaskGridItem extends StatelessWidget {
  final String taskId;
  final Function()? onLongPress;
  final bool isRoot;

  const TaskGridItem(
    this.taskId,
    this.onLongPress, {
    this.isRoot = false,
  });

  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<SettingsProvider>(context, listen: false);
    final tm = Provider.of<TimemanagerProvider>(context);
    final taskTimeTracker = Provider.of<TaskTimeTrackerProvider>(context);
    final task = tm.get<Task>(taskId)!;
    return TimemanagerObjectGridItem(
      timeMangerObject: task,
      onTap: () async {
        await showModalBottomSheet(
          elevation: 4,
          context: context,
          builder: (ctx) {
            return GestureDetector(
              onTap: () {},
              child: TimeEntriesModal(task.uuid),
              behavior: HitTestBehavior.opaque,
            );
          },
        );
      },
      onLongPress: onLongPress,
      leadingIcon: task.uuid == taskTimeTracker.currentTask
          ? Icon(
              Icons.fiber_manual_record,
              size: 30,
              color: Colors.red,
            )
          : Icon(
              Icons.assignment_turned_in_outlined,
              size: 30,
            ),
      titleText: task.name +
          (isRoot ? ' - ' + task.project.name + ' - ' + task.client.name : ''),
      subtitle: Container(
        height: 90,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text('general.time_entries'.tl(context) +
                ': ' +
                task.times.length.toString()),
            Text('general.today'.tl(context) +
                ': ' +
                task
                    .getDurationBetween(DateTime.now().getStartOfDay(),
                        DateTime.now().getEndOfDay())
                    .toFormattedString(
                        showDays: Provider.of<SettingsProvider>(context,
                                listen: false)
                            .showDays)),
            Text('general.this_week'.tl(context) +
                ': ' +
                task
                    .getDurationBetween(
                        DateTime.now()
                            .getStartOfWeek(startOfWeek: settings.startOfWeek),
                        DateTime.now()
                            .getEndOfWeek(startOfWeek: settings.startOfWeek))
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.last_week'.tl(context) +
                ': ' +
                task
                    .getDurationBetween(
                        DateTime.now().getStartOfWeek(
                            weekOffset: -1, startOfWeek: settings.startOfWeek),
                        DateTime.now().getEndOfWeek(
                            weekOffset: -1, startOfWeek: settings.startOfWeek))
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.total'.tl(context) +
                ': ' +
                task.duration.toFormattedString(showDays: settings.showDays)),
          ],
        ),
      ),
      onShare: () => ShareTimes.share(task.times.toList(), context),
    );
  }
}
