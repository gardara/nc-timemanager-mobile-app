import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:provider/provider.dart';

import '../helper/duration_format.dart';
import '../helper/i18n_helper.dart';
import '../model/project.dart';
import '../model/task.dart';
import '../provider/settings_provider.dart';
import '../provider/task_time_tracker_provider.dart';
import '../provider/timemanager_provider.dart';
import '../widgets/app_drawer.dart';

class TaskTimeTrackerScreen extends StatefulWidget {
  static const routeName = '/task/tracker';

  @override
  _TaskTimeTrackerScreenState createState() => _TaskTimeTrackerScreenState();
}

class _TaskTimeTrackerScreenState extends State<TaskTimeTrackerScreen> {
  late Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final taskTimeTracker = Provider.of<TaskTimeTrackerProvider>(context);
    final appBar = AppBar(
      title: Text('general.time_tracker'.tl(context)),
    );
    return Scaffold(
        drawer: AppDrawer(),
        appBar: appBar,
        body: Builder(
          builder: (context) => taskTimeTracker.isActive
              ? _buildActiveTrackerView(taskTimeTracker)
              : _buildInActiveTrackerView(),
        ));
  }

  Widget _buildActiveTrackerView(TaskTimeTrackerProvider taskTimeTracker) {
    final tm = Provider.of<TimemanagerProvider>(context, listen: false);
    final task = tm.get<Task>(taskTimeTracker.currentTask!)!;
    final project = task.project;
    final client = project.client;

    return SingleChildScrollView(
      child: Container(
        height: math.max(
            MediaQuery.of(context).size.height -
                MediaQuery.of(context).viewInsets.vertical -
                80,
            400),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              client.name,
              textAlign: TextAlign.center,
              textScaleFactor: 1.5,
            ),
            Text(
              project.name,
              textAlign: TextAlign.center,
              textScaleFactor: 1.5,
            ),
            Text(
              task.name,
              textAlign: TextAlign.center,
              textScaleFactor: 1.5,
            ),
            Text(
              taskTimeTracker.activeSince.toFormattedString(
                  showSeconds: true,
                  showDays:
                      Provider.of<SettingsProvider>(context, listen: false)
                          .showDays),
              textAlign: TextAlign.center,
              textScaleFactor: 2.5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: TextFormField(
                initialValue: taskTimeTracker.note,
                decoration: InputDecoration(
                  labelText: 'general.note'.tl(context),
                ),
                onChanged: (value) => taskTimeTracker.note = value,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (!taskTimeTracker.paused)
                  TextButton.icon(
                    onPressed: () {
                      taskTimeTracker.pause();
                      setState(() {});
                    },
                    icon: Icon(Icons.pause),
                    label: Text(
                      'general.pause'.tl(context),
                      textScaleFactor: 1.4,
                    ),
                  ),
                if (taskTimeTracker.paused)
                  TextButton.icon(
                    onPressed: () {
                      taskTimeTracker.resume();
                      setState(() {});
                    },
                    icon: Icon(Icons.play_arrow),
                    label: Text(
                      'general.resume'.tl(context),
                      textScaleFactor: 1.4,
                    ),
                  ),
                if (taskTimeTracker.paused)
                  TextButton.icon(
                    onPressed: () async => await _cancel(taskTimeTracker),
                    icon: Icon(Icons.cancel),
                    label: Text(
                      MaterialLocalizations.of(context).cancelButtonLabel,
                      textScaleFactor: 1.4,
                    ),
                  ),
                if (!taskTimeTracker.paused)
                  TextButton.icon(
                    onPressed: () async => await _end(taskTimeTracker),
                    icon: Icon(Icons.stop_rounded),
                    label: Text(
                      'general.save'.tl(context),
                      textScaleFactor: 1.5,
                    ),
                  ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildInActiveTrackerView() {
    return Center(
        child: Text(
      'time_tracker_screen.no_active_task'.tl(context),
      textScaleFactor: 2.0,
      textAlign: TextAlign.center,
    ));
  }

  Future<void> _cancel(TaskTimeTrackerProvider taskTimeTracker) async {
    final result = await showDialog<bool>(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('dialog.are_you_sure'.tl(context)),
              content:
                  Text('time_tracker_screen.stop_time_tracking'.tl(context)),
              actions: [
                TextButton(
                  child:
                      Text(MaterialLocalizations.of(context).cancelButtonLabel),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                TextButton(
                  child: Text(MaterialLocalizations.of(context).okButtonLabel),
                  onPressed: () => Navigator.of(context).pop(true),
                ),
              ],
            ));
    if (result != null && result) {
      taskTimeTracker.cancel();
    }
  }

  Future<void> _end(TaskTimeTrackerProvider taskTimeTracker) async {
    final result = await showDialog<bool>(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('dialog.are_you_sure'.tl(context)),
              content:
                  Text('time_tracker_screen.save_time_tracking'.tl(context)),
              actions: [
                TextButton(
                  child:
                      Text(MaterialLocalizations.of(context).cancelButtonLabel),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                TextButton(
                  child: Text(MaterialLocalizations.of(context).okButtonLabel),
                  onPressed: () => Navigator.of(context).pop(true),
                ),
              ],
            ));
    if (result != null && result) {
      taskTimeTracker
          .end(Provider.of<TimemanagerProvider>(context, listen: false));
    }
  }
}
