import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helper/i18n_helper.dart';
import '../model/abstract_timemanager_object.dart';
import '../model/client.dart';
import '../provider/timemanager_provider.dart';
import '../widgets/app_drawer.dart';
import '../widgets/grid_items/client_grid_item.dart';
import '../widgets/search_grid.dart';

class ClientOverviewScreen extends StatefulWidget {
  static const routeName = '/client/overview';

  @override
  _ClientOverviewScreenState createState() => _ClientOverviewScreenState();
}

class _ClientOverviewScreenState extends State<ClientOverviewScreen> {
  var _searchString = '';

  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);
    List<Client> clients = tm.getAll<Client>().toList();
    clients.sort((one, other) =>
        one.name.toLowerCase().compareTo(other.name.toLowerCase()));
    if (_searchString.isNotEmpty) {
      clients = clients
          .where(
              (c) => c.name.toLowerCase().contains(_searchString.toLowerCase()))
          .toList();
    }
    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        title: Text('general.clients'.tl(context)),
        actions: [
          IconButton(icon: Icon(Icons.add), onPressed: updateClient),
        ],
      ),
      body: SearchGrid(
        childCount: clients.length,
        builder: (context, i) =>
            ClientGridItem(clients[i].uuid, () => updateClient(clients[i])),
        onSearchChanged: (s) {
          setState(() {
            _searchString = s;
          });
        },
        onRefresh: () => tm.sync(),
      ),
    );
  }

  void updateClient([Client? client]) async {
    String name = client == null ? '' : client.name;
    String note = client == null
        ? ''
        : client.note == null
            ? ''
            : client.note!;
    await showDialog<String>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(client == null
            ? 'client_screen.create_client'.tl(context)
            : 'client_screen.edit_client'.tl(context)),
        content: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      initialValue: name,
                      decoration: InputDecoration(
                        labelText: 'general.name'.tl(context),
                      ),
                      onChanged: (value) => name = value,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      initialValue: note,
                      decoration: InputDecoration(
                        labelText: 'general.note'.tl(context),
                      ),
                      onChanged: (value) => note = value,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        actions: [
          TextButton(
            onPressed: () {
              name = '';
              Navigator.of(context).pop();
            },
            child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(MaterialLocalizations.of(context).okButtonLabel),
          ),
        ],
      ),
    );
    if (name.isNotEmpty) {
      final tm = Provider.of<TimemanagerProvider>(context, listen: false);
      if (client == null) {
        tm.create(Client(tm, name: name, note: note));
      } else {
        tm.update(Client.copy(
          CopyBehaviour.updateCurrent,
          tm,
          client,
          name: name,
          note: note,
        ));
      }
      tm.sync();
    }
  }
}
