import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:week_of_year/week_of_year.dart';

import './date_time_helper.dart';
import './i18n_helper.dart';
import '../model/client.dart';
import '../model/project.dart';
import '../model/task.dart';
import '../model/time.dart';

enum PeriodMode { Day, Week, Month, Year }

class AccumulatedDuration implements Comparable {
  final BuildContext _context;
  final PeriodMode mode;
  final Duration duration;
  final Time _relatedTimeEntry;
  late String _label;
  late String _key;

  String get label => _label;

  String get key => _key;

  AccumulatedDuration(
      this._context, this.mode, this.duration, this._relatedTimeEntry) {
    final key = generateKey(mode, _relatedTimeEntry);
    this._key = key;
    switch (mode) {
      case PeriodMode.Day:
        this._label =
            DateFormat.yMEd(Localizations.localeOf(_context).languageCode)
                .format(_relatedTimeEntry.end);
        break;
      case PeriodMode.Week:
        this._label = _relatedTimeEntry.end.weekOfYear.toString() +
            ' ' +
            'dashboard.chart.week_in'.tl(_context) +
            ' ' +
            _relatedTimeEntry.end.year.toString();
        break;
      case PeriodMode.Month:
        this._label =
            DateFormat.yM(Localizations.localeOf(_context).languageCode)
                .format(_relatedTimeEntry.end);
        break;
      case PeriodMode.Year:
        this._label = _relatedTimeEntry.end.year.toString();
        break;
    }
  }

  AccumulatedDuration operator +(AccumulatedDuration other) {
    if (_key != other._key)
      throw ArgumentError('The keys "$_key" and "${other._key} must be equal');
    if (mode != other.mode)
      throw ArgumentError('The modes "$mode" and "${other.mode} must be equal');

    return AccumulatedDuration(
        _context, mode, duration + other.duration, _relatedTimeEntry);
  }

  @override
  int compareTo(other) {
    if (!(other is AccumulatedDuration)) return 0;
    return (other as AccumulatedDuration)._key.compareTo(this._key);
  }

  static String generateKey(PeriodMode mode, Time time) {
    switch (mode) {
      case PeriodMode.Day:
        return time.end.getStartOfDay().toIso8601String();
      case PeriodMode.Week:
        return time.end.year.toString() +
            time.end.weekOfYear.toString().padLeft(3, '0');
      case PeriodMode.Month:
        return time.end.year.toString() +
            time.end.month.toString().padLeft(2, '0');
      case PeriodMode.Year:
        return time.end.year.toString();
    }
    return '';
  }
}

extension ClientInPeriodExtension on Client {
  Duration getDurationInPeriod(PeriodMode mode, String referenceKey) =>
      getTimesInPeriod(mode, referenceKey).fold<Duration>(
        Duration(),
        (previousValue, time) => previousValue + time.duration,
      );

  Set<Time> getTimesInPeriod(PeriodMode mode, String referenceKey) => this
      .times
      .where(
          (time) => AccumulatedDuration.generateKey(mode, time) == referenceKey)
      .toSet();
}

extension ProjectInPeriodExtension on Project {
  Duration getDurationInPeriod(PeriodMode mode, String referenceKey) =>
      getTimesInPeriod(mode, referenceKey).fold<Duration>(
        Duration(),
        (previousValue, time) => previousValue + time.duration,
      );

  Set<Time> getTimesInPeriod(PeriodMode mode, String referenceKey) => this
      .times
      .where(
          (time) => AccumulatedDuration.generateKey(mode, time) == referenceKey)
      .toSet();
}

extension TaskInPeriodExtension on Task {
  Duration getDurationInPeriod(PeriodMode mode, String referenceKey) =>
      getTimesInPeriod(mode, referenceKey).fold<Duration>(
        Duration(),
        (previousValue, time) => previousValue + time.duration,
      );

  Set<Time> getTimesInPeriod(PeriodMode mode, String referenceKey) => this
      .times
      .where(
          (time) => AccumulatedDuration.generateKey(mode, time) == referenceKey)
      .toSet();
}
