import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';

import './provider/nextcloud_auth_provider.dart';
import './provider/settings_provider.dart';
import './provider/task_time_tracker_provider.dart';
import './provider/theme_provider.dart';
import './provider/timemanager_provider.dart';
import './screens/client_overview_screen.dart';
import './screens/dashboard_screen.dart';
import './screens/nextcloud_auth_screen.dart';
import './screens/project_overview_screen.dart';
import './screens/settings_screen.dart';
import './screens/task_overview_screen.dart';
import './screens/task_time_tracker_screen.dart';
import './screens/time_entries_screen.dart';

Future<void> main() async {
  final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
    translationLoader: FileTranslationLoader(
      useCountryCode: false,
      fallbackFile: 'en',
      basePath: 'assets/i18n',
      //forcedLocale: Locale('de'),
    ),
  );
  //await flutterI18nDelegate.load();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(NCTimemanagerMobileApp(flutterI18nDelegate));
}

class NCTimemanagerMobileApp extends StatelessWidget {
  final FlutterI18nDelegate flutterI18nDelegate;

  NCTimemanagerMobileApp(this.flutterI18nDelegate);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SettingsProvider>(
          create: (context) => SettingsProvider(),
        ),
        ChangeNotifierProvider<NextcloudAuthProvider>(
          create: (ctx) => NextcloudAuthProvider(),
        ),
        ChangeNotifierProvider<TaskTimeTrackerProvider>(
          create: (context) => TaskTimeTrackerProvider(),
        ),
        ChangeNotifierProxyProvider<NextcloudAuthProvider, TimemanagerProvider>(
          create: (context) => TimemanagerProvider(null),
          update: (context, ncAuth, previous) =>
              previous!.hasNcProvider ? previous : TimemanagerProvider(ncAuth),
        ),
        ChangeNotifierProxyProvider2<NextcloudAuthProvider, SettingsProvider,
            ThemeProvider>(
          create: (context) => ThemeProvider(null, null),
          update: (context, ncAuth, settings, previous) =>
              ThemeProvider(ncAuth, settings),
        )
      ],
      //builder: FlutterI18n.rootAppBuilder(),
      child: Builder(
        builder: (context) => _buildApp(context),
      ),
    );
  }

  Widget _buildApp(BuildContext context) {
    final webAuth = Provider.of<NextcloudAuthProvider>(context, listen: false);
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final settings = Provider.of<SettingsProvider>(context, listen: false);
    return MaterialApp(
      title: 'NC Timemanager',
      localizationsDelegates: [
        flutterI18nDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('de'),
        const Locale('fr'),
        const Locale('cs'),
      ],
      theme: Provider.of<ThemeProvider>(context).currentTheme(false),
      darkTheme: Provider.of<ThemeProvider>(context).currentTheme(true),
      routes: {
        SettingsScreen.routeName: (ctx) => SettingsScreen(),
        DashboardScreen.routeName: (ctx) => DashboardScreen(),
        ClientOverviewScreen.routeName: (ctx) => ClientOverviewScreen(),
        ProjectOverviewScreen.routeName: (ctx) => ProjectOverviewScreen(),
        TaskOverviewScreen.routeName: (ctx) => TaskOverviewScreen(),
        TaskTimeTrackerScreen.routeName: (ctx) => TaskTimeTrackerScreen(),
        TimeEntriesScreen.routeName: (ctx) => TimeEntriesScreen(),
      },
      home: FutureBuilder(
        future: settings.loadFromStorage(webAuth, themeProvider),
        builder: (ctx2, snapshot) {
          return snapshot.connectionState == ConnectionState.done
              ? Root()
              : Scaffold(
                  backgroundColor: Colors.black,
                  body: Center(child: CircularProgressIndicator()));
        },
      ),
    );
  }
}

class Root extends StatefulWidget {
  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {
  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
  }

  @override
  Widget build(BuildContext context) {
    final webAuth = Provider.of<NextcloudAuthProvider>(context, listen: false);
    final settings = Provider.of<SettingsProvider>(context, listen: false);
    return !webAuth.isAuthenticated
        ? NextcloudAuthScreen()
        : _loadHome(settings.startView);
  }

  Widget _loadHome(StartView startView) {
    switch (startView) {
      case StartView.Dashboard:
        {
          return DashboardScreen();
        }
      case StartView.Clients:
        {
          return ClientOverviewScreen();
        }
      case StartView.Projekts:
        {
          return ProjectOverviewScreen();
        }
      case StartView.Tasks:
        {
          return TaskOverviewScreen();
        }
      case StartView.TimeEntries:
        {
          return TimeEntriesScreen();
        }
      default:
        {
          return DashboardScreen();
        }
    }
  }
}
