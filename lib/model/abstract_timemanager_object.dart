import 'dart:convert';

import 'package:uuid/uuid.dart';

import 'timemanager.dart';

enum CopyBehaviour { createNew, updateCurrent }

abstract class AbstractTimemanagerObject {
  static final _uuidGenerator = Uuid();
  final Timemanager timemanager;
  final String uuid;
  final String? commit;
  final DateTime? created;
  final DateTime? changed;

  AbstractTimemanagerObject(
    this.timemanager, {
    String? uuid,
    String? commit,
    DateTime? created,
    DateTime? changed,
  })  : uuid = uuid ?? _uuidGenerator.v4(),
        commit = commit ?? _uuidGenerator.v4(),
        created = created ?? DateTime.now(),
        changed = changed ?? DateTime.now();

  AbstractTimemanagerObject.fromMap(
      Timemanager timemanager, Map<String, dynamic> jsonMap)
      : this(
    timemanager,
          uuid: jsonMap['uuid'],
          commit: jsonMap['commit'],
          created: DateTime.parse(jsonMap['created']),
          changed: DateTime.parse(jsonMap['changed']),
        );

  Map<String, dynamic> toMap() =>
      {
        'uuid': uuid,
        'commit': commit,
        'created': created == null ? null : created?.toIso8601String(),
        'changed': changed == null ? null : changed?.toIso8601String(),
      };

  @override
  String toString() {
    return toMap().toString();
  }

  String toJsonString() {
    return json.encode(toMap());
  }

  Set<AbstractTimemanagerObject> get dependentChildObjects;

  @override
  bool operator ==(Object other) {
    return other is AbstractTimemanagerObject && other.uuid == uuid;
  }

  @override
  int get hashCode => uuid.hashCode;
}
