import 'abstract_timemanager_object.dart';
import 'project.dart';
import 'task.dart';
import 'time.dart';
import 'timemanager.dart';

class Client extends AbstractTimemanagerObject {
  final String? city;
  final String? email;
  final String name;
  final String? note;
  final String? phone;
  final String? postcode;
  final String? street;
  final String? web;

  Client(
    Timemanager timemanager, {
    String? uuid,
    String? commit,
    DateTime? created,
    DateTime? changed,
    this.city,
    this.email,
    required this.name,
    this.note,
    this.phone,
    this.postcode,
    this.street,
    this.web,
  })  : assert(name.isNotEmpty),
        super(
          timemanager,
          uuid: uuid,
          commit: commit,
          created: created,
          changed: changed,
        );

  Client.fromMap(Timemanager timemanager, Map<String, dynamic> jsonMap)
      : this(
          timemanager,
          uuid: jsonMap['uuid'],
          commit: jsonMap['commit'],
          created: DateTime.parse(jsonMap['created']),
          changed: DateTime.parse(jsonMap['changed']),
          city: jsonMap['city'],
          email: jsonMap['email'],
          name: jsonMap['name'],
          note: jsonMap['note'],
          phone: jsonMap['phone'],
          postcode: jsonMap['postcode'],
          street: jsonMap['street'],
          web: jsonMap['web'],
        );

  Client.copy(
    CopyBehaviour copyBehaviour,
    Timemanager timemanager,
    Client client, {
    String? city,
    String? email,
    String? name,
    String? note,
    String? phone,
    String? postcode,
    String? street,
    String? web,
  }) : this(
          timemanager,
          uuid: copyBehaviour == CopyBehaviour.createNew ? null : client.uuid,
          commit:
              copyBehaviour == CopyBehaviour.createNew ? null : client.commit,
          created: copyBehaviour == CopyBehaviour.createNew
              ? DateTime.now()
              : client.created,
          changed: DateTime.now(),
          city: city ?? client.city,
          email: email ?? client.email,
          name: name ?? client.name,
          note: note ?? client.note,
          phone: phone ?? client.phone,
          postcode: postcode ?? client.postcode,
          street: street ?? client.street,
          web: web ?? client.web,
        );

  @override
  Map<String, dynamic> toMap() => super.toMap()
    ..addAll(
      {
        'city': city,
        'email': email,
        'name': name,
        'note': note,
        'phone': phone,
        'postcode': postcode,
        'street': street,
        'web': web,
      },
    );

  @override
  Set<AbstractTimemanagerObject> get dependentChildObjects {
    var children = <AbstractTimemanagerObject>{};
    children.addAll(projects);
    children.addAll(tasks);
    children.addAll(times);
    return children;
  }
}

extension TimeDuration on Client {
  Duration get duration => projects.fold<Duration>(
        Duration(),
        (previousValue, element) => previousValue + element.duration,
      );

  Duration getDurationBetween(DateTime start, DateTime end,
          {List<Project> projects = const []}) =>
      times
          .where((time) =>
              time.start.isAfter(start) &&
              time.start.isBefore(end) &&
              (projects.isEmpty || projects.contains(time.project.uuid)))
          .fold<Duration>(Duration(),
              (previousValue, time) => previousValue + time.duration);
}

extension RelationExtensions on Client {
  Set<Project> get projects => timemanager
      .getAll<Project>()
      .where(
        (project) => project.client_uuid == uuid,
      )
      .toSet();

  Set<Task> get tasks => projects.expand((project) => project.tasks).toSet();

  Set<Time> get times => tasks.expand((task) => task.times).toSet();
}
