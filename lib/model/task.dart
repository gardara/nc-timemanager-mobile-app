import 'abstract_timemanager_object.dart';
import 'client.dart';
import 'project.dart';
import 'time.dart';
import 'timemanager.dart';

class Task extends AbstractTimemanagerObject {
  final String name;
  final String project_uuid;

  Task(
    Timemanager timemanager, {
    String? uuid,
    String? commit,
    DateTime? created,
    DateTime? changed,
    required this.name,
    required this.project_uuid,
  })   : assert(name.isNotEmpty),
        assert(project_uuid.isNotEmpty),
        super(
          timemanager,
          uuid: uuid,
          commit: commit,
          created: created,
          changed: changed,
        );

  Task.fromMap(Timemanager timemanager, Map<String, dynamic> jsonMap)
      : this(
          timemanager,
          uuid: jsonMap['uuid'],
          commit: jsonMap['commit'],
          created: DateTime.parse(jsonMap['created']),
          changed: DateTime.parse(jsonMap['changed']),
          name: jsonMap['name'],
          project_uuid: jsonMap['project_uuid'],
        );

  Task.copy(
    CopyBehaviour copyBehaviour,
    Timemanager timemanager,
    Task task, {
    String? name,
    required String project_uuid,
  }) : this(
    timemanager,
          uuid: copyBehaviour == CopyBehaviour.createNew ? null : task.uuid,
          commit: copyBehaviour == CopyBehaviour.createNew ? null : task.commit,
          created: copyBehaviour == CopyBehaviour.createNew
              ? DateTime.now()
              : task.created,
          changed: DateTime.now(),
          name: name ?? task.name,
          project_uuid: project_uuid,
        );

  @override
  Map<String, dynamic> toMap() => super.toMap()
    ..addAll(
      {
        'name': name,
        'project_uuid': project_uuid,
      },
    );

  @override
  Set<AbstractTimemanagerObject> get dependentChildObjects => times;
}

extension TimeDuration on Task {
  Duration get duration => times.fold<Duration>(
      Duration(), (previousValue, element) => previousValue + element.duration);

  Duration getDurationBetween(DateTime start, DateTime end) => times
      .where((time) => time.start.isAfter(start) && time.start.isBefore(end))
      .fold<Duration>(
          Duration(), (previousValue, time) => previousValue + time.duration);
}

extension RelationExtensions on Task {
  Client get client => timemanager.get<Client>(project.client_uuid)!;

  Project get project => timemanager.get<Project>(project_uuid)!;

  Set<Time> get times => timemanager
      .getAll<Time>()
      .where(
        (time) => time.task_uuid == uuid,
      )
      .toSet();

  List<Time> get timesOrdered =>
      times.toList()..sort((a, b) => a.start.compareTo(b.start));
}
