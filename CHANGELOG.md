# Changelog

## 21.12.0

- Keyboard fix

## 21.4.0

- Copy time (swipe time list item to the right)
- Bugfix (Weekly statistic bar chart sorting)

## 21.3.0

- Add barcode login (app password)
- Sorting days in chart fixed
- Time entries screen (+filter by timespan)

## 21.2.1

- Add export functionality (csv)

## 21.2.0

- Updated Charts, filter per selected period
- Updated grid item
- Enter the time as duration or in time from-to format

## 21.1.0

- Dashboard
    - See your last tasks
    - Charts: Visualization of time durations for clients, projects, task per day, week, month and
      year
- Time tracker for tasks
- Settings for default duration

## 20.12.0

- Create/Update/Delete clients/projects/tasks/times